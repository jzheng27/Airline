import MySQLdb as mdb

hostname = 'localhost'
username = 'root'
password = 'mypassword'
dbname = 'airlines'
port = 3306


def fetch_all_SU():
	db = mdb.connect(host=hostname, port = port, user=username, passwd = password, db=dbname)
	cursor = db.cursor()
	cursor.execute("SELECT * FROM suRequest")
	data = cursor.fetchall()
	db.close()
	return data

def insert_SU(start, dest, date, price):
	db = mdb.connect(host=hostname, port = port, user=username, passwd = password, db=dbname)
	cursor = db.cursor()
	try:
		cursor.execute("INSERT INTO suRequest(start, dest, date, price) VALUES(%s, %s, %s, %s)", [start, dest, date, price])
		db.commit()
	except:
		db.rollback()
		print "Inserting failed"
	db.close()

def check_SU(start, dest, date):
	db = mdb.connect(host=hostname, port = port, user=username, passwd = password, db=dbname)
	cursor = db.cursor()
	cursor.execute("SELECT * from suRequest where start = %s and dest = %s and date = %s", [start, dest, date])
	data = cursor.fetchall()
	return len(data) > 0

def update_price(start, dest, date, price):
	db = mdb.connect(host=hostname, port = port, user=username, passwd = password, db=dbname)
	cursor = db.cursor()
	cursor.execute("SELECT * from suRequest where start = %s and dest = %s and date = %s", [start, dest, date])
	data = cursor.fetchone()
	if data[4] != price:
		try:
			cursor.execute("UPDATE suRequest SET price = %s WHERE start = %s and dest = %s and date = %s", [price, start, dest, date])
			db.commit()
		except:
			db.rollback()
			print "update failed"
	db.close()



if __name__=='__main__':
	print fetch_all_SU()