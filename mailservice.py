import smtplib
from email.mime.text import MIMEText
import hashlib


GMAIL = "junyu.lance.zheng@gmail.com"
GPASS = "Zjy960606"


def generate_verify(username, to):
	hashed = hashlib.sha224(username+to).hexdigest()

	text = """Enter the following URL to activate your email:
	http://localhost:5000/account/verify?username={}&email={}&hashed={}
	""".format(username, to, hashed)

	msg = MIMEText(text)
	msg['Subject'] = "Please Activate Your Email"
	msg['From'] = GMAIL
	msg['To'] = to

	s = smtplib.SMTP("smtp.gmail.com", port = 587)
	s.ehlo()
	s.starttls()
	s.login(GMAIL, GPASS)
	s.sendmail(GMAIL, [to], msg.as_string())
	s.quit()

def true_hash(username, to):
	return hashlib.sha224(username+to).hexdigest()

def send_su_update(to, start, dest, date, old_price, new_price):
	text = "The flight from {} to {} on {} has got lowest price moved from {} to {}".format(start, dest, date, old_price, new_price)

	msg = MIMEText(text)
	msg['Subject'] = "Price Change"
	msg['From'] = GMAIL 
	msg['To'] = to

	s = smtplib.SMTP("smtp.gmail.com", port = 587)
	s.ehlo()
	s.starttls()
	s.login(GMAIL, GPASS)
	s.sendmail(GMAIL, [to], msg.as_string())
	s.quit()


if __name__ == '__main__':
	generate_verify('jzheng27@illinois.edu')
