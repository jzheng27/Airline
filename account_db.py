import MySQLdb as mdb

db_hostname = 'localhost'
db_username = 'root'
db_password = 'mypassword'
dbname = 'airlines'
port = 3306


def add_user(username, password, email):
    db = mdb.connect(host=db_hostname, port = port, user=db_username, passwd = db_password, db=dbname)
    cursor = db.cursor()
    success = True
    try:
        cursor.execute("INSERT INTO account(username, password, email) VALUES(%s, %s, %s)", [username, password, email])
        db.commit()
    except:
        db.rollback()
        success = False
        print "Inserting failed"
    db.close()

    return success


def change_password(username, oldpass, newpass):
    db = mdb.connect(host=db_hostname, port = port, user=db_username, passwd = db_password, db=dbname)
    cursor = db.cursor()
    success = False
    cursor.execute("SELECT * FROM account WHERE username = %s AND password = %s", [username, oldpass])
    if len(cursor.fetchall()) == 1:
        try:
            cursor.execute("UPDATE account SET password = %s WHERE username = %s", [newpass, username])
            db.commit()
            success = True
        except:
            db.rollback()
    db.close()

    return success


def verify_email(username, email):
    db = mdb.connect(host=db_hostname, port = port, user=db_username, passwd = db_password, db=dbname)
    cursor = db.cursor()
    success = False
    cursor.execute("SELECT * from account WHERE username = %s AND email = %s", [username, email])
    if len(cursor.fetchall()) == 1:
        try:
            cursor.execute("UPDATE account SET verified = 1 WHERE username = %s", [username])
            db.commit()
            success = True
        except:
            db.rollback()
            
    db.close()

    return success


def check_login(username, password):
    db = mdb.connect(host=db_hostname, port = port, user=db_username, passwd = db_password, db=dbname)
    cursor = db.cursor()
    success = False
    cursor.execute("SELECT * from account WHERE username = %s AND password = %s", [username, password])
    if len(cursor.fetchall()) == 1:
        success = True
    db.close()
    return success


def fetch_all():
    db = mdb.connect(host=db_hostname, port = port, user=db_username, passwd = db_password, db=dbname)
    cursor = db.cursor()
    cursor.execute("SELECT * FROM account")
    data = cursor.fetchall()
    db.close()
    return data


if __name__=='__main__':
    print fetch_all()
