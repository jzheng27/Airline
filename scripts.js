

var searchApp = angular.module("searchApp", []);
searchApp.controller("searchCtrl", function($scope, $http) {
	$scope.start = "ORD";
	$scope.dest = "LAX";
	$scope.date = "2018-01-20";
	$scope.websites = [];

	$scope.displayName = "init";
	$scope.username = "thirduser";
	$scope.password = "thirdpassword";

	$scope.doSearch = function() {

		var Url = $scope.getQuery();

		var req = {
			method : "GET",
			url: Url
		}

		console.log("here");
		$http(req).then(function(response) {
			$scope.price = response.data["price"];
			$scope.websites = [{name:"Website Name", price: "Price"}];
			$scope.websites = $scope.websites.concat(
						[{name: 'Student Universe', price: response.data["su"]},
						{name: 'Expedia', price: response.data["ex"]}]);
		});

	}

	$scope.getQuery = function() {
		return "http://localhost:5000/search/all?from=" + $scope.start + 
		"&to=" + $scope.dest + "&date=" + $scope.date ;
	}

	$scope.login = function() {

		username = $scope.username;
		password = $scope.password;


		var url = "http://localhost:5000/account/login?password="+password+"&username="+username
		

		$http.post(url).then(function(response) {
			result = response.data["result"];
			console.log(response.data);
			if (result) {
				$scope.displayName = username;
				$scope.loggedin = true;
			}
		});

	}

	$scope.checklogin = function() {
		var req = {
			method : "GET",
			url: "http://localhost:5000/account/loggedin",

		};

		
		$http(req).then(function(response) {

			console.log(response.data);

		});
	}
});



