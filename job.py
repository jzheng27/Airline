import studentuniverse as su
import expedia as ex
import airlines_db as adb
import expedia_db as edb
import focus_db as fdb
import mailservice as ms

searches = adb.fetch_all_SU()

for search in searches:
	start = search[1]
	dest = search[2]
	date = search[3]
	price = search[4]

	new_price = su.get_price_by_condition(start, dest, date)

	if price != new_price:
		adb.update_price(start, dest, date, new_price)
		print "updated", start, dest, date, price
		emails = fdb.get_emails_su(start, dest, date)
		for email in emails:
			print "sending to {}".format(email)
			ms.send_su_update(email, start, dest, date, price, new_price)



searches = edb.fetch_all_SU()

for search in searches:
	start = search[1]
	dest = search[2]
	date = search[3]
	price = search[4]

	new_price = ex.get_price_by_condition(start, dest, date)

	if price != new_price:
		edb.update_price(start, dest, date, new_price)
		print "updated", start, dest, date, price
		emails = fdb.get_emails_ex(start, dest, date)
		for email in emails:
			print "sending to {}".format(email)
			ms.send_ex_update(email, start, dest, date, price, new_price)

