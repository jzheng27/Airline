from flask import Flask
from flask import request
from flask import jsonify
from flask import session
from flask import Response


import studentuniverse as su
import expedia as ex
import airlines_db as adb
import expedia_db as edb
import account_db as acdb
import focus_db as fdb
import mailservice as ms

import string

app = Flask(__name__)

app.secret_key = 'super secret key'
app.config['SESSION_TYPE'] = 'filesystem'

last_user = None

def format_date(suDate):
    date_arr = suDate.split('-')
    new_arr = [date_arr[1], date_arr[2], date_arr[0]]
    return "/".join(new_arr)



@app.route("/search/all")
def get_all_time():
    start = request.args.get('from')
    dest = request.args.get('to')
    date = request.args.get('date')

    price1 = su.get_price_by_condition(start, dest, date)

    if not adb.check_SU(start, dest, date):
        adb.insert_SU(start, dest, date, price1)

    adb.update_price(start, dest, date, price1)

    username = last_user

    if 'logged_in' in session.keys():
        status = session['logged_in']

        if status:
            username = session['user']

    fdb.add_su_focus(username, start, dest, date)
########

    date = format_date(date)

    price2 = ex.get_price_by_condition(start, dest, date)

    if not edb.check(start, dest, date):
        edb.insert(start, dest, date, price2)

    edb.update_price(start, dest, date, price2)
    
    username = last_user

    if 'logged_in' in session.keys():
        status = session['logged_in']

        if status:
            username = session['user']

    fdb.add_ex_focus(username, start, dest, date)


    resp = jsonify({'su' : price1, 'ex': price2})
    resp.headers['Access-Control-Allow-Origin'] = '*'

    return resp


@app.route("/search/studentuniverse")
def get_su_time():
    start = request.args.get('from')
    dest = request.args.get('to')
    date = request.args.get('date')

    price = su.get_price_by_condition(start, dest, date)

    if not adb.check_SU(start, dest, date):
        adb.insert_SU(start, dest, date, price)

    adb.update_price(start, dest, date, price)

    username = last_user

    if 'logged_in' in session.keys():
        status = session['logged_in']

        if status:
            username = session['user']


    fdb.add_su_focus(username, start, dest, date)


    resp = jsonify({'price' : price})
    resp.headers['Access-Control-Allow-Origin'] = '*'

    return resp



@app.route("/search/expedia")
def get_ex_time():
    start = request.args.get('from')
    dest = request.args.get('to')
    date = request.args.get('date')

    date = format_date(date)

    price = ex.get_price_by_condition(start, dest, date)

    if not edb.check(start, dest, date):
        edb.insert(start, dest, date, price)

    edb.update_price(start, dest, date, price)
    
    username = last_user

    if 'logged_in' in session.keys():
        status = session['logged_in']

        if status:
            username = session['user']

    fdb.add_ex_focus(username, start, dest, date)


    resp = jsonify({'price' : price})
    resp.headers['Access-Control-Allow-Origin'] = '*'

    return resp



@app.route("/account/register", methods = ['POST'])
def register():
    json_data = request.json
    username=json_data['username']
    password=json_data['password']
    email=json_data['email']

    if acdb.add_user(username, password, email):
        status = 'success'
        ms.generate_verify(username, email)
    else:
        status = 'this user is already registered'

    resp = jsonify({"result": status})
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp


@app.route("/account/change_password", methods = ['POST', 'OPTIONS'])
def change_password():
    json_data = request.json
    username=json_data['username']
    oldpass=json_data['oldpass']
    newpass=json_data['newpass']

    if acdb.change_password(username, oldpass, newpass):
        status = 'success'
    else:
        status = 'password change failed'

    resp = jsonify({"result": status})
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Access-Control-Allow-Headers'] = 'Content-Type'
    resp.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
    return resp


@app.route('/account/login', methods=['POST', 'GET', 'OPTIONS'])
def login():
    try:
        json_data = request.get_json()
        print json_data
        username=json_data['username']
        password=json_data['password']
    except TypeError:
        username = request.args.get('username')
        password = request.args.get('password')
    print username, password
    last_user = username

    if acdb.check_login(username, password):
        session['logged_in'] = True
        session['user'] = username
        status = True
    else:
        status = False
    resp = jsonify({"result": status})
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Access-Control-Allow-Headers'] = 'Content-Type'
    resp.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
    return resp

@app.route('/account/loggedin')
def loggedin():
    status = False
    if 'logged_in' in session.keys():
        status = session['logged_in']
    result = {"result": status}
    if status:
        result['user'] = session['user']
    resp = jsonify(result)
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

@app.route('/account/logout')
def logout():
    session.pop('logged_in', None)
    session.pop('user', None)
    resp = jsonify({'result': 'success'})
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

@app.route('/account/verify')
def verify():
    username = request.args.get('username')
    email = request.args.get('email')
    hashed = request.args.get('hashed')


    true_hash = ms.true_hash(username, email)
    if hashed == true_hash:
        if acdb.verify_email(username, email):
            return "Successfully verified!"
        else:
            return "DB not pass."
    else: 
        return "Bad hash."
    return "Verification failed!"

