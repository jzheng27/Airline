import MySQLdb as mdb


db_hostname = 'localhost'
db_username = 'root'
db_password = 'mypassword'
dbname = 'airlines'
port = 3306


def add_su_focus(username, start, dest, date):
    db = mdb.connect(host=db_hostname, port = port, user=db_username, passwd = db_password, db=dbname)
    cursor = db.cursor()
    success = False

    try: 
        cursor.execute("SELECT id FROM account WHERE username = %s", [username])
        (account_id,) = cursor.fetchone()

        cursor.execute("SELECT id FROM suRequest WHERE start = %s AND dest = %s AND date = %s", [start, dest, date])
        (su_id,) = cursor.fetchone()
    except:
        return False

    try: 
        cursor.execute("INSERT INTO focus(account_id, su_id) VALUES(%s, %s)", [account_id, su_id])
        db.commit()
        success = True
    except:
        db.rollback()

    db.close()
    return success

def add_ex_focus(username, start, dest, date):
    db = mdb.connect(host=db_hostname, port = port, user=db_username, passwd = db_password, db=dbname)
    cursor = db.cursor()
    success = False

    try: 
        cursor.execute("SELECT id FROM account WHERE username = %s", [username])
        (account_id,) = cursor.fetchone()

        cursor.execute("SELECT id FROM exRequest WHERE start = %s AND dest = %s AND date = %s", [start, dest, date])
        (ex_id,) = cursor.fetchone()
    except:
        return False

    try: 
        cursor.execute("INSERT INTO focus(account_id, ex_id) VALUES(%s, %s)", [account_id, ex_id])
        db.commit()
        success = True
    except:
        db.rollback()

    db.close()
    return success

def get_emails_su(start, dest, date):
    db = mdb.connect(host=db_hostname, port = port, user=db_username, passwd = db_password, db=dbname)
    cursor = db.cursor()

    try:
        cursor.execute("""SELECT a.email FROM account a, suRequest su, focus f 
                        WHERE a.email IS NOT NULL AND a.verified >= 1 AND
                            a.id = f.account_id AND f.su_id = su.id AND 
                            su.start = %s AND su.dest = %s AND su.date = %s"""
                        , [start, dest, date])
        emails = [email for (email,) in cursor.fetchall()]
        emails = set(emails)
        emails = list(emails)
        return emails
    except:
        return None

def get_emails_ex(start, dest, date):
    db = mdb.connect(host=db_hostname, port = port, user=db_username, passwd = db_password, db=dbname)
    cursor = db.cursor()

    try:
        cursor.execute("""SELECT a.email FROM account a, exRequest ex, focus f 
                        WHERE a.email IS NOT NULL AND a.verified >= 1 AND
                            a.id = f.account_id AND f.ex_id = ex.id AND 
                            ex.start = %s AND ex.dest = %s AND ex.date = %s"""
                        , [start, dest, date])
        emails = [email for (email,) in cursor.fetchall()]
        emails = set(emails)
        emails = list(emails)
        return emails
    except:
        return None

if __name__=='__main__':
    # print add_su_focus('tests', 'ORD', 'LAX', '2017-12-26')
    # print get_emails_su('ORD', 'LAX', '2017-12-26')
    print add_ex_focus('tests', 'ORD', 'SFO', '01/20/2018')
    print get_emails_ex('ORD', 'SFO', '01/20/2018')



