from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys

import re 


class NoSuchTicketError(Exception):
	def __init__(self, msg):
		self.message = msg
	def __str__(self):
		return repr(self.value)

def get_price_by_condition(start, dest, date):
	url = "https://www.expedia.com"

	driver = webdriver.Chrome()


	driver.get(url)

	element = WebDriverWait(driver, 10).until(
			EC.presence_of_element_located((By.XPATH,
						 "//button[@data-lob='flight']"))
	)
	element.click()

	element = WebDriverWait(driver, 10).until(
		EC.presence_of_element_located((By.XPATH,
					 "//label[@id='flight-type-one-way-label-hp-flight']"))
	)
	element.click()

	element = WebDriverWait(driver, 10).until(
		EC.presence_of_element_located((By.XPATH,
					 "//input[@data-airport_code_element='flight-origin-hp-flight-airport_code']"))
	)
	element.send_keys(start)

	element = WebDriverWait(driver, 10).until(
		EC.presence_of_element_located((By.XPATH,
					 "//input[@data-airport_code_element='flight-destination-hp-flight-airport_code']"))
	)
	element.send_keys(dest)

	element = WebDriverWait(driver, 10).until(
		EC.presence_of_element_located((By.XPATH,
					 "//input[@id='flight-departing-single-hp-flight']"))
	)
	element.send_keys(date)
	element.send_keys(Keys.RETURN)


	element = WebDriverWait(driver, 10).until(
		EC.presence_of_element_located((By.XPATH,
					 "//div[@class='fill' and @style='width: 100%;']"))
	)

	element = WebDriverWait(driver, 10).until(
		EC.presence_of_element_located((By.XPATH,
					 "//span[@class='dollars price-emphasis']"))
	)

	ret = re.sub('\D', '', element.text) 

	driver.quit()

	return int(ret)

if __name__ == '__main__':
	print get_price_by_condition('ORD', 'LAX', '12/27/2017')