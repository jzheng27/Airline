CREATE DATABASE airlines;

USE airlines;

CREATE TABLE suRequest (
	id INT AUTO_INCREMENT NOT NULL,
	start VARCHAR(20) NOT NULL,
	dest VARCHAR(20) NOT NULL,
	date VARCHAR(20) NOT NULL,
	price int,
	PRIMARY KEY (id)
);

CREATE TABLE exRequest (
	id INT AUTO_INCREMENT NOT NULL,
	start VARCHAR(20) NOT NULL,
	dest VARCHAR(20) NOT NULL,
	date VARCHAR(20) NOT NULL,
	price int,
	PRIMARY KEY (id)
);

CREATE TABLE account (
	id INT AUTO_INCREMENT NOT NULL,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	email VARCHAR(100),
	verified BOOL DEFAULT 0,
	PRIMARY KEY (id),
	UNIQUE (username)
);

CREATE TABLE focus (
	id INT AUTO_INCREMENT,
	account_id INT NOT NULL,
	su_id INT,
	ex_id INT,
	PRIMARY KEY (id),
	FOREIGN KEY (account_id) REFERENCES account(id),
	FOREIGN KEY (su_id) REFERENCES suRequest(id),
	FOREIGN KEY (ex_id) REFERENCES exRequest(id),
	UNIQUE (account_id, su_id)
);