from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
import re 
from pyvirtualdisplay import Display

class NoSuchTicketError(Exception):
	def __init__(self, msg):
		self.message = msg
	def __str__(self):
		return repr(self.value)

def get_price_by_condition(start, dest, date):

	url = "https://www.studentuniverse.com/flights/1/{_from}/{_to}/{_date}?flexible=false&premiumCabins=false".format(
					_from = start, 
					_to = dest,
					_date = date) 

	driver = webdriver.Chrome()

	fault_count = 0

	while True:
		try: 
			driver.get(url)
			element = WebDriverWait(driver, 100).until(
					EC.presence_of_element_located((By.XPATH,
								 "//li[@context='suRecommends' and @parameter1='price']"))
				)
			element = element.find_element_by_xpath(".//h2")
			price_tag = element.text
			break

		except (TimeoutException, NoSuchElementException):
			fault_count += 1
			if fault_count > 5:
				driver.quit()
				raise NoSuchTicketError("No such ticket found!")
			continue

	driver.quit()
	
	ret = re.sub('\D', '', price_tag)
	return int(ret)

if __name__ == '__main__':
	print get_price_by_condition('CHI', 'PEK', '2017-12-25')